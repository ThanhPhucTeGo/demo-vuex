import Vue from 'vue'
import App from './App.vue'
import store from './store'

Vue.config.productionTip = false
Vue.prototype.$log = console.log

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
