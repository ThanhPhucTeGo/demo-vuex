const state = {
    auth: {
        authentication: true
    }
}
const getters = {
	authentication: state => state.auth.authentication
}
const mutations = {
    LOGIN(state) {
        state.auth.authentication = true
    },
    LOGOUT(state) {
        state.auth.authentication = false
    },
}
export default {
    state,
    getters,
    mutations
}