import axios from 'axios'
const todoModule = {
    state: {
        todoList: [],
    },
    getters: {
        todoList: state => state.todoList,
        doneTodos: state => state.todoList.filter(done => done.completed),
        progress: (state, getters) => {
            const doneTodo = getters.doneTodos.length
            return Math.round((doneTodo / state.todoList.length) * 100)
        },
    },
    mutations: {

        MARK_COMPLETED(state, todoId) {
            state.todoList.map(todo => {
                if(todo.id === todoId) todo.completed = !todo.completed
                return todo
            }) 
        },
        DELETE_TODO (state, todoId) {
            state.todoList = state.todoList.filter(item => item.id !== todoId)
        },
        ADD_TODO (state, newTodo) {
            state.todoList.push(newTodo)
        },
        SET_TODOS (state, todos){
            state.todoList = todos
        }
    },
    actions: {
        async deleteTodo(context, todoId){
            try {
                await axios.delete(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
                context.commit("DELETE_TODO", todoId)
            } catch (error) {
                console.log(error)
            }
        },
        async addTodo(context, newToDo){
            try {
                await axios.post(`https://jsonplaceholder.typicode.com/todos`, newToDo)
                context.commit("ADD_TODO", newToDo)
            } catch (error) {
                console.log(error)
            }
        },
        async getTodos({commit}){
            try {
                const response = await axios.get('https://jsonplaceholder.typicode.com/todos?_limit=5')
                commit('SET_TODOS', response.data)
            } catch (error) {
                console.log(error)
            }
        }
    }
}

export default todoModule